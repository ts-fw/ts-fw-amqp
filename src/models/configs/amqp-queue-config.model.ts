import {Options} from 'amqplib';
import AssertQueue = Options.AssertQueue;

export interface IAMQPQueueConfigModel {
    name: string;
    isDelayed?: boolean;
    options?: AssertQueue;
}
