import {IConnectionManagerConnectionOptions} from '../definitions';
import {IAMQPQueueConfigModel} from './amqp-queue-config.model';

export interface IAMQPOptionsConfigModel {
    urls: string[];
    connectionOptions?: IConnectionManagerConnectionOptions;
    queues?: IAMQPQueueConfigModel[];
}
