import {ConfirmChannel, Connection} from 'amqplib';
import {EventEmitter} from 'events';

export interface IChannelWrapperOnErrorParams {
    error: Error;
    data: {name: string};
}

export interface OnConnectionOptions {
    url: string;
    connection: Connection;
}

export interface IAMQPConnectionOptions {
    keepAlive: boolean;
    keepAliveDelay: number;
    noDelay: boolean;
    timeout: number;
}

export interface IConnectionManagerConnectionOptions {
    heartbeatIntervalInSeconds: number;
    reconnectTimeInSeconds: number;
    connectionOptions: IAMQPConnectionOptions;
}

export interface IChannelWrapperOptions {
    name: string;
    json?: boolean;
    setup?: (channel: ConfirmChannel) => void;
}

export interface ChannelWrapper extends EventEmitter {
    constructor(connectionManager: AMQPConnectionManager, options?: IChannelWrapperOptions);
    removeSetup(): Promise<boolean>;
    queueLength(): number;
    close(): Promise<boolean>;
    waitForConnect(): Promise<void>;
    ack(...args): Promise<void>;
    nack(...args): Promise<void>;
    publish(exchange: string, routingKey: string, content: any, options?: any): Promise<void>;
    sendToQueue(exchange: string, content: any, options?: any): Promise<void>;
    on(event: 'error', listener: (params: IChannelWrapperOnErrorParams | Error) => void);
}

export interface AMQPConnectionManager extends EventEmitter {
    new(urls: string[], connectionOptions?: IConnectionManagerConnectionOptions);
    createChannel(options?: IChannelWrapperOptions): Promise<ChannelWrapper>;
    close(): Promise<void>;
    isConnected(): boolean;
    on(event: 'disconnect', callback: (params: {err: Error}) => void);
    on(event: 'connect', listener: (params: OnConnectionOptions) => void);
}
