import {ConfirmChannel, Options} from 'amqplib';
import {ChannelWrapper} from './amqp-connection-manager';
import Publish = Options.Publish;

export interface IAMQPDelayNodeOptions {
    separator?: string;
    prefix?: string;
    threshold?: string;
    round?: string;
}

export interface DelayedChannel {
    publish(exchange: string, routingKey: string, content: any, options: Publish): Promise<void>;
}

export interface DelayChannelWrapper extends ChannelWrapper {
    delay(delayMs: number): DelayedChannel;
}

export interface AMQPDelayNode {
    (channel: ConfirmChannel, options?: IAMQPDelayNodeOptions);
}
