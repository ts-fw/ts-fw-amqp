import {Options} from 'amqplib';
import AssertExchange = Options.AssertExchange;

export enum EExchangeType {
    DIRECT = 'direct',
    TOPIC = 'topic',
    HEADERS = 'headers',
    FAN_OUT = 'fanout'
}

export interface IExchange {
    name: string;
    type: EExchangeType;
    options: AssertExchange;
}

export function Exchange(name: string,
                         type: EExchangeType = EExchangeType.FAN_OUT,
                         options?: AssertExchange): ClassDecorator {
    return (constructor: Function) => {
        let exchanges: IExchange[];
        if (Reflect.hasMetadata('exchanges', constructor)) {
            exchanges = Reflect.getMetadata('exchanges', constructor);
        } else {
            exchanges = [];
            Reflect.defineMetadata('exchanges', exchanges, constructor);
        }

        exchanges.push({
            name: name,
            type: type,
            options: options
        });
    }
}
