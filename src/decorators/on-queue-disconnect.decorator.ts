export function OnQueueDisconnect(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnQueueDisconnect', propertyKey, constructor);
    }
}
