import {Options} from 'amqplib';

export function OnQueueMessage(options?: Options.Consume): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnQueueMessage', propertyKey, constructor);
        Reflect.defineMetadata('options', options, constructor);
    }
}
