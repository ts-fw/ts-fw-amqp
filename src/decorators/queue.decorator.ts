import {Injectable} from 'ts-fw';
import {Replies} from 'amqplib';
import AssertQueue = Replies.AssertQueue;

export function Queue(name: string, isDelayed: boolean = false, options?: AssertQueue): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([Injectable('Queue')], constructor);
        Reflect.defineMetadata('name', name, constructor.prototype);
        Reflect.defineMetadata('isDelayed', isDelayed, constructor.prototype);
        Reflect.defineMetadata('options', options, constructor.prototype);
    }
}
