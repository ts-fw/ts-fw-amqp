export function OnQueueConnect(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnQueueConnect', propertyKey, constructor);
    }
}
