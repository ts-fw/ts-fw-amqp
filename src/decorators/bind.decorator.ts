export interface IBind {
    destination: string;
    source: string;
    pattern: string;
    args?: any;
}

export function Bind(destination: string, source: string, pattern: string, args?: any): ClassDecorator {
    return (constructor: Function) => {
        let bindings: IBind[];
        if (Reflect.hasMetadata('bindings', constructor)) {
            bindings = Reflect.getMetadata('bindings', constructor);
        } else {
            bindings = [];
            Reflect.defineMetadata('bindings', bindings, constructor);
        }

        bindings.push({
            destination: destination,
            source: source,
            pattern: pattern,
            args: args
        });
    }
}
