import {inject, Container} from 'inversify';
import {Config, IConfigStorage} from 'ts-fw';
import * as winston from 'winston';
import {IAMQPOptionsConfigModel} from '../models';
import {IBind} from '../decorators';
import {DelayChannelWrapper} from '../models';
import {Options} from 'amqplib';
import * as AMQPConnectionManager from 'amqp-connection-manager';
import * as amqpDelayNode from 'amqp-delay.node';
import * as camelcase from 'camelcase';
import AssertQueue = Options.AssertQueue;

@Config(2)
export class AMQPConfiguratorConfig {
    @inject('logger')
    private logger: winston.Winston;

    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('di')
    private container: Container;

    configuring() {
        const amqpOptionsConfig = this.configStorage.get<IAMQPOptionsConfigModel>('amqp');

        const queues: any[] = [];
        try {
            queues.push(...this.container.getAll<Function>('Queue'));
        } catch (ignore) {
            this.logger.debug(
                'ts-fw-amqp',
                'Queues not found. Use @Queue() decorator for create mark of class as queue service'
            );
        }

        const connectionManager = AMQPConnectionManager.connect(
            amqpOptionsConfig.urls,
            amqpOptionsConfig.connectionOptions
        );
        this.container.bind('amqpConnectionManager').toConstantValue(connectionManager);

        connectionManager.on('connect', async params => {
            this.logger.debug('ts-fw-amqp', `Connected to AMQP server by address ${params.url} complete`);
            this.container.bind('amqpConnection').toConstantValue(params.connection);

            if (amqpOptionsConfig.queues) {
                for (const queue of amqpOptionsConfig.queues) {
                    const queueName = queue.name;
                    const isDelayed = queue.isDelayed || false;
                    const options = queue.options;

                    const channelWrapper = await connectionManager.createChannel({
                        json: true, name: queueName,
                        setup: async channel => {
                            await channel.assertQueue(queueName, options);

                            if (isDelayed === true) {
                                amqpDelayNode(channel);
                            }

                            this.container.bind(`${camelcase(queueName)}AMQPChannel`).toConstantValue(channel);
                        }
                    });

                    this.container.bind(`${camelcase(queueName)}AMQPChannelWrapper`).toConstantValue(channelWrapper);
                }
            }

            queues.forEach(async queue => {
                if (Reflect.hasMetadata('OnQueueConnect', queue)) {
                    const method: string = Reflect.getMetadata('OnQueueConnect', queue);
                    await queue[method]();
                }

                const queueName: string = Reflect.getMetadata('name', queue);
                const options: AssertQueue = Reflect.getMetadata('options', queue);

                const channelWrapper = await connectionManager.createChannel({
                    json: true,
                    name: queueName,
                    setup: async channel => {
                        await channel.assertQueue(queueName, options);

                        const isDelayed: boolean = Reflect.getMetadata('isDelayed', queue);
                        let delayedChannel: DelayChannelWrapper;

                        if (isDelayed === true) {
                            amqpDelayNode(channel);
                            delayedChannel = <any>channel;
                        }

                        this.container
                            .bind(`${camelcase(queueName)}AMQPChannel`)
                            .toConstantValue(channel);

                        if (Reflect.hasMetadata('OnQueueMessage', queue)) {
                            const methodName: string = Reflect.getMetadata('OnQueueMessage', queue);
                            if (methodName && typeof queue[methodName] === 'function') {
                                await channel.consume(queueName, async message => {
                                    if (isDelayed) {
                                        await queue[methodName](message, delayedChannel);
                                        return;
                                    }

                                    await queue[methodName](message, channelWrapper);
                                });
                            }
                        }

                        const bindings: IBind[] = [];
                        if (Reflect.hasMetadata('bindings', queue)) {
                            bindings.push(...Reflect.getMetadata('exchanges', queue));
                        }

                        for (const binding of bindings) {
                            try {
                                await channel.bindExchange(
                                    binding.destination,
                                    binding.source,
                                    binding.pattern,
                                    binding.args
                                );
                                this.logger.debug(
                                    'ts-fw-amqp',
                                    `Binding from ${binding.source} to ${binding.destination} was created`
                                );
                            } catch (error) {
                                this.logger.error(error);
                                this.logger.debug(
                                    'ts-fw-amqp',
                                    `Binding from ${binding.source} to ${binding.destination} already exists`
                                );
                            }
                        }
                    }
                });

                this.container
                    .bind(`${camelcase(queueName)}AMQPChannelWrapper`)
                    .toConstantValue(channelWrapper);
            });
        });

        connectionManager.on('disconnect', params => {
            queues.forEach(async queue => {
                if (Reflect.hasMetadata('OnQueueDisconnect', queue)) {
                    const methodName: string = Reflect.getMetadata('OnQueueDisconnect', queue);
                    await queue[methodName](params);
                }
            });
        });
    }
}
